<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {
            $table->increments('tax_id');
            $table->text('tax_name')->nullable();
            $table->text('tax_rate')->nullable();
            $table->text('tax_zone')->nullable();
            $table->text('based_on')->nullable();
            $table->text('tax_rule')->nullable();
            $table->integer('tax_order')->default('0');
            $table->integer('tax_status')->default('0');
            $table->string('tax_date',10)->nullable();
            $table->string('tax_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
