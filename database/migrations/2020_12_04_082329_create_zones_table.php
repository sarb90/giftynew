<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones', function (Blueprint $table) {
            $table->increments('zone_id');
            $table->text('zone_name')->nullable();
            $table->text('zone_code')->nullable();
            $table->integer('zone_country')->default('0');
            $table->integer('zone_state')->default('0');
            $table->integer('zone_city')->default('0');
            $table->integer('zone_order')->default('0');
            $table->integer('zone_status')->default('0');
            $table->string('zone_date',10)->nullable();
            $table->string('zone_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zones');
    }
}
