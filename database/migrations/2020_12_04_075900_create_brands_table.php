<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('brand_id');
            $table->text('brand_name')->nullable();
            $table->text('brand_slug')->nullable();
            $table->text('seokey')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('brand_image')->nullable();
            $table->integer('brand_order')->default('0');
            $table->integer('brand_status')->default('0');
            $table->string('brand_date',10)->nullable();
            $table->string('brand_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}
