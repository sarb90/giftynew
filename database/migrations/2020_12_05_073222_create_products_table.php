<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('pro_id')->nullable();
            $table->text('pro_slug')->nullable();
            $table->text('product_title')->nullable();
            $table->text('product_short_description')->nullable();
            $table->text('seokey')->nullable();
            $table->text('product_tag')->nullable();
            $table->text('metatag')->nullable();
            $table->text('upc_no')->nullable();
            $table->text('upcseries')->nullable();
            $table->text('product_sku')->nullable();
            $table->text('product_model')->nullable();
            $table->text('product_category')->nullable();
            $table->text('product_subcategory')->nullable();
            $table->text('product_category2')->nullable();
            $table->text('product_subcategory2')->nullable();
            $table->text('product_brand')->nullable();
            $table->text('product_manfacture')->nullable();
            $table->text('packageup_image')->nullable();
            $table->text('product_description')->nullable();
            $table->text('product_specfic')->nullable();
            $table->text('product_data')->nullable();
            $table->text('cost_price')->nullable();
            $table->text('discount_price')->nullable();
            $table->text('multi_price')->nullable();
            $table->text('saving_price')->nullable();
            $table->text('regular_price')->nullable();
            $table->text('sell_price')->nullable();
            $table->text('tax_price')->nullable();
            $table->text('product_inventory')->nullable();
            $table->integer('quantity')->default('0');
            $table->text('product_condition')->nullable();
            $table->text('product_warning')->nullable();
            $table->text('manfacture_date')->nullable();
            $table->text('manfacture_date_view')->nullable();
            $table->text('product_discount')->nullable();
            $table->text('discount_qty')->nullable();
            $table->text('discount')->nullable();
            $table->text('weight_name')->nullable();
            $table->text('weight_label')->nullable();
            $table->text('weight_value')->nullable();
            $table->text('product_main_discount')->nullable();
            $table->text('shipping_weight')->nullable();
            $table->text('shipping_length')->nullable();
            $table->text('shipping_width')->nullable();
            $table->text('shipping_height')->nullable();
            $table->text('product_reward')->nullable();
            $table->text('ship_country_name')->nullable();
            $table->text('ship_state_name')->nullable();
            $table->text('ship_city_name')->nullable();
            $table->text('shipstatus')->nullable();
            $table->text('ship_services')->nullable();
            $table->text('ship_cost')->nullable();
            $table->text('shipadditional')->nullable();
            $table->text('product_attribute')->nullable();
            $table->text('handling_time')->nullable();
            $table->integer('pro_status')->default('0');
            $table->integer('pro_order')->default('0');
            $table->text('product_related')->nullable();
            $table->text('pro_date')->nullable();
            $table->text('pro_time')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
