<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_services', function (Blueprint $table) {
            $table->increments('ship_id');
            $table->text('ship_title')->nullable();
            $table->text('ship_color')->nullable();
            $table->integer('ship_order')->default('0');
            $table->integer('ship_status')->default('0');
            $table->string('ship_date',10)->nullable();
            $table->string('ship_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_services');
    }
}
