<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\media;
use Illuminate\Support\Facades\DB;
use session;
use Cookie;


class MediaController extends Controller
{
     public function media(Request $request)
    {
    	$data['title'] = "Media";
    	return view('admin.media',compact('data'));
    }
    public function loadfile(Request $request)
    {
    	$medfile=media::where('delete_status','1')->limit(10)->get();
    	return view('admin.loadfile',compact('medfile'));
    }
    public function nxtloadfile(Request $request)
    {
        $medfile=media::where('delete_status','1')->skip(11)->take(500)->get();
        return view('admin.nxtloadfile',compact('medfile'));
    }
  
   


    public function media_upload(Request $request)
    {
    	$baseurl="http://localhost/hirola/";
    	 if (!empty($_FILES)) {
    	 	$image=$request->file('file');
    	 	$extension=strtolower($request->file->getClientOriginalExtension());
    	 

    	 	if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
			{
				 // move_uploaded_file($_FILES['file']['tmp_name'],  'public/media/image/' . $_FILES['file']['name']);
				 $po_logoname = time().'.'.$request->file('file')->getClientOriginalExtension();

					// request()->item_image->storeAs(public_path('consultant-images'), $imageName);
				$dir = 'public/media/image';

				$request->file('file')->move($dir, $po_logoname);
		        
			}
			else
			{
				$po_logoname = "";
			}
	       
			$data = [
		            'path' => $baseurl.'public/media/image/',
		            'filename' => $po_logoname,
		        ];
	        // echo 'asd';
	        // print_r($data);

	        // echo 'sad';
	        date_default_timezone_set('Asia/Kolkata');
			$create_date=date("Y-m-d");
			$create_time=date("h:i:sa");
	        $dataset=new media;
	        $dataset->path='public/media/image/';
	        $dataset->image_name=$po_logoname;
	        $dataset->create_date=$create_date;
	        $dataset->create_time=$create_time;
	        $dataset->save();
	        echo json_encode($data);
	    }
    }

    public function media_delete(Request $request)
    {
        $filename=$request->get('filename');

        $medfile=media::where('image_name',$filename)->update(array('delete_status' => 0));

    }
}
