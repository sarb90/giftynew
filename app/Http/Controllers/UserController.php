<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\UsersModel;
use App\Models\User;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Catalog;
use App\Models\Subcatalog;
use Session;
use Socialite;
use App\Services\SocialFacebookAccountService;
use Exception;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        $data['title'] = "Home";
        return view('index',compact('data'));
    }
    public static function srch_vendor($users_id)
    {
        $srch_vendor=User::Where('id',$users_id)->first();
        return $srch_vendor;
    }
    public function register_login()
    {
        if(session()->has('users_id'))
        {
            return redirect()->intended('/');
        }
        else
        {
            $data['title'] = "Register/Login";
            $countries = Country::all();
            return view('users.register-login',compact('data','countries'));
        }
    }
    public function register()
    {
    	if(session()->has('users_id'))
        {
        	return redirect()->to('/');
        }
        else
        {
            $data['title'] = "Register";
	        $countries = Country::all();
	        return view('users.register',compact('data','countries'));
        }
	}
	public function captchacode(Request $request)
    {
    	$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	return substr(str_shuffle($permitted_chars), 0, 6);	
    }
    public function email_check(Request $request)
    {   
        $user = User::where('email', '=' , $request['email'])->first();
        if (!empty($user)) 
        {
            return "exist";
        }
        else
        {
            return "noexist";
        }
    }
    public function phone_check(Request $request)
    {   
        $user = User::where('phone', '=' , $request['phone'])->first();
        if (!empty($user)) 
        {
            return "exist";
        }
        else
        {
            return "noexist";
        }
    }
    public function register_user(Request $request)
    {   
		$UsersModel = new UsersModel;
		$UsersModel->name=$request->name;
		$UsersModel->last_name=$request->last_name;
		$UsersModel->email=$request->email;
		$UsersModel->phone=$request->phone;
		$UsersModel->company_name=$request->company_name;
		$UsersModel->country_id=$request->country_id;
		$UsersModel->i_am_a=$request->i_am_a;
		$UsersModel->password=md5($request->password);
		$user = UsersModel::where('email', '=' , $request['email'])->first();
		if (!empty($user)) 
		{
			return ['status' => 'exist', 'redirect' => '', 'msg' =>'This email already exist'];
		}
		else
		{
            $user1 = UsersModel::where('phone', '=' , $request['phone'])->first();
            if (!empty($user1)) 
            {
                return ['status' => 'exist', 'redirect' => '', 'msg' =>'This phone already exist'];
            }
            else
            {
    			$usermodel=$UsersModel->save();
    			if($usermodel)
    			{
    				return ['status' => 'success', 'redirect' => 'register', 'msg' =>'Successfully Registered'];
    			}
    			else
    			{
    				return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while registeration'];
    			}
            }
		}
    }
    public function login()
    {
    	if(session()->has('users_id'))
        {
        	return redirect()->to('/');
        }
        else
        {
            $data['title'] = "Login";
        	return view('users.login',compact('data'));
        }
	}
	public function login_user(Request $request)
    {   
		$user = User::where('email', '=' , $request->emails)->first();
		if (!empty($user)) 
		{
			if($user->password!=md5($request->passwords))
			{
				return ['status' => 'invalid', 'redirect' => '', 'msg' =>'Invalid Login'];
			}
			else
			{
				$request->session()->put('users_id',$user->id);
                if($user->i_am_a=="supplier" || $user->i_am_a=="both")
                {
                    return ['status' => 'success', 'redirect' => 'catalog', 'msg' =>'Successfully Login'];
                }
                else
                {
                    return ['status' => 'success', 'redirect' => '/', 'msg' =>'Successfully Login'];
                }
			}
		}
		else
		{
			return ['status' => 'invalid', 'redirect' => '', 'msg' =>'Invalid Login'];
		}
    }
	public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function callback(SocialFacebookAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
        Session::put('users_id',$user->id);
        return redirect()->to('/');
        // auth()->login($user);
    }
    public function redirectgmail()
    {
        return Socialite::driver('google')->redirect();
    }
    public function callbackgmail()
    {
        try 
        {
	        $googleUser = Socialite::driver('google')->user();
	        $existUser = User::where('email',$googleUser->email)->first();
            if($existUser) 
            {
            	Session::put('users_id',$existUser->id);
                // Auth::loginUsingId($existUser->id);
            }
            else 
            {
                $user = new User;
                $user->name = $googleUser->name;
                $user->email = $googleUser->email;
                $user->google_id = $googleUser->id;
                $user->password = md5(rand(1,10000));
                $user->save();
                $existUser = User::where('email',$googleUser->email)->first();
                Session::put('users_id',$existUser->id);
                // Auth::loginUsingId($user->id);
            }
            return redirect()->to('/');
        } 
        catch (Exception $e) 
        {
            return 'error';
        }
    }
    /*public function redirectlinkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }
    public function callbacklinkedin()
    {
        try 
        {
            $linkdinUser = Socialite::driver('linkedin')->user();
            $existUser = UsersModel::where('email',$linkdinUser->email)->first();
            if($existUser) 
            {
                // Auth::loginUsingId($existUser->id);
                Session::put('users_id',$existUser->id);
            }
            else 
            {
                $user = new UsersModel;
                $user->name = $linkdinUser->name;
                $user->email = $linkdinUser->email;
                $user->linkedin_id = $linkdinUser->id;
                $user->password = md5(rand(1,10000));
                $user->save();
                // Auth::loginUsingId($user->id);
                Session::put('users_id',$user->id);
            }
            return redirect()->to('/login');
        } 
        catch (Exception $e) 
        {
            return 'error';
        }
    }*/
    public function my_account()
    {
        if(session()->has('users_id'))
        {
            $users_id = Session::get('users_id');
            $userdata=User::where('id',$users_id)->first();
            $countries = Country::all();
            $data['title'] = "My Account";
            return view('users.my-account',compact('data','userdata','countries'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function email_check1(Request $request)
    {   
        $users_id = Session::get('users_id');
        $user = User::where('email',$request['email'])->where('id', '!=' , $users_id)->first();
        if (!empty($user)) 
        {
            return "exist";
        }
        else
        {
            return "noexist";
        }
    }
    public function phone_check1(Request $request)
    {   
        $users_id = Session::get('users_id');
        $user = User::where('phone', '=' , $request['phone'])->where('id', '!=' , $users_id)->first();
        if (!empty($user)) 
        {
            return "exist";
        }
        else
        {
            return "noexist";
        }
    }
    public function my_account_user(Request $request)
    {   
        $users_id = Session::get('users_id');
        $user = User::where('email', '=' , $request['email'])->where('id', '!=' , $users_id)->first();
        if (!empty($user)) 
        {
            return ['status' => 'exist', 'redirect' => '', 'msg' =>'This email already exist'];
        }
        else
        {
            $user1 = UsersModel::where('phone', '=' , $request['phone'])->where('id', '!=' , $users_id)->first();
            if (!empty($user1)) 
            {
                return ['status' => 'exist', 'redirect' => '', 'msg' =>'This phone already exist'];
            }
            else
            {
                $updatedata=array(
                    "name"=>$request->name,
                    "last_name"=>$request->last_name,
                    "email"=>$request->email,
                    "phone"=>$request->phone,
                    "company_name"=>$request->company_name,
                    "country_id"=>$request->country_id,
                    "i_am_a"=>$request->i_am_a,
                );
                $user = User::where('id',$users_id)->update($updatedata);
                if($user == 1)
                {
                    return ['status' => 'success', 'redirect' => 'register', 'msg' =>'Successfully Updated'];
                }
                else
                {
                    return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while updation'];
                }
            }
        }
    }
    public function shop(Request $request)
    {
        $data['title'] = "Shop";
        $branddata=Brand::where('brand_status','1')->orderBy('brand_order','ASC')->get();
        $product_cat=Catalog::where('cat_status','1')->orderBy('cat_order','ASC')->get();
        $product=Product::where('pro_status','1')->orderBy('pro_order','ASC')->limit(20)->paginate(20);
        return view('shop',compact('data','branddata','product_cat','product'));
    }
    public static function srch_scat($cat_id)
    {
        $srch_scat=Subcatalog::where('scat_status','1')->where('cat_id',$cat_id)->orderBy('scat_order','ASC')->get();
        return $srch_scat;
    }
    public function searchproduct_filter(Request $request)
    {
        $brand=$request->get('tbrand');
        $category=$request->get('tcat_id');
        $subcategory=$request->get('tsubcate_id');
        $brand_array=array();
        $category_array=array();
        $subcategory_array=array();
        if($brand!='')
        {
            $brand_array=explode(',',$brand);
        }
        if($category!='')
        {
            $category_array=explode(',',$category);
        }
        if($subcategory !='')
        {
            $subcategory_array=explode(',',$subcategory);
        }
        $newresults=array();
        $data_frontend="";
        $product=Product::where('pro_status','1');
        if(!empty($brand_array))
        {
            for($br=0;$br<count($brand_array);$br++)
            { 
                $product=$product->where('product_brand','like', '%' . $brand_array[$br] . '%');
            }
        }
        if(!empty($category_array))
        {
            for($ck=0;$ck<count($category_array);$ck++)
            {
                $product=$product->where('product_category',$category_array[$ck]);
            }
        }
        if(!empty($subcategory_array))
        {
            for($sk=0;$sk<count($subcategory_array);$sk++)
            {
                $product=$product->where('product_subcategory',$subcategory_array[$sk]);
            }
        }
       $product= $product->orderBy('pro_order','ASC')->get();
       $message='';
        foreach ($product as $prolist) 
        {
           $pro_images=explode(",",$prolist->packageup_image);
            if(empty($pro_images))
            {
                $poimage='assets/1-9.jpg';
            }
            else
            {
                $poimage='product_image/'.$pro_images[0].'';
            }
            $message.='<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="single-product-wrap mb-35">
                                <div class="product-img product-img-zoom mb-15">
                                    <a href="product-details.html">
                                        <img src="'.asset($poimage).'" alt="'.$prolist->product_title.'">
                                    </a>
                                    <div class="product-action-2 tooltip-style-2">
                                        <button title="Wishlist"><i class="icon-heart"></i></button>
                                        <button title="Quick View" data-toggle="modal" data-target="#exampleModal"><i class="icon-size-fullscreen icons"></i></button>
                                        <button title="Compare"><i class="icon-refresh"></i></button>
                                    </div>
                                </div>
                                <div class="product-content-wrap-2 text-center">
                                    <div class="product-rating-wrap">
                                        <div class="product-rating">
                                            <i class="icon_star"></i>
                                            <i class="icon_star"></i>
                                            <i class="icon_star"></i>
                                            <i class="icon_star"></i>
                                            <i class="icon_star gray"></i>
                                        </div>
                                        <span>(2)</span>
                                    </div>
                                    <h3><a href="product-details.html">'.$prolist->product_title.'</a></h3>
                                    <div class="product-price-2">
                                        <span>$'.sprintf('%0.2f', $prolist->regular_price).'</span>
                                    </div>
                                </div>
                                <div class="product-content-wrap-2 product-content-position text-center">
                                    <div class="product-rating-wrap">
                                        <div class="product-rating">
                                            <i class="icon_star"></i>
                                            <i class="icon_star"></i>
                                            <i class="icon_star"></i>
                                            <i class="icon_star"></i>
                                            <i class="icon_star gray"></i>
                                        </div>
                                        <span>(2)</span>
                                    </div>
                                    <h3><a href="product-details.html">'.$prolist->product_title.'</a></h3>
                                    <div class="product-price-2">
                                        <span>$'.sprintf('%0.2f', $prolist->regular_price).'</span>
                                    </div>
                                    <div class="pro-add-to-cart">
                                        <button title="Add to Cart">Add To Cart</button>
                                    </div>
                                </div>
                            </div>
                        </div>';
        }
        echo $message;
        
    }
    public function logout()
    {
        session()->forget('users_id');
        return redirect()->intended('/');
    }

    //cart
    public function cart()
    {
        $data['title'] = "Cart";
        return view('cart',compact('data'));
    }
    public function checkout()
    {
        $data['title'] = "checkout";
        return view('checkout',compact('data'));
    }
     public function shop_fullwide()
    {
        $data['title'] = "shop_fullwide";
        return view('shop_fullwide',compact('data'));
    }
     public function product_details()
    {
        $data['title'] = "product_details";
        return view('product_details',compact('data'));
    }
}
