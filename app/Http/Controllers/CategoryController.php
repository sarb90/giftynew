<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog;
use Redirect;
use File;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    //
    public function index()
    {
		$images = \File::allFiles(public_path('/product/catalog'));
        return view('/admin/add-category',compact('images'));
    }
    public function add_category(Request $request)
    {
    //    echo "<pre>";
	// 	print_r($request->all());
	// 	die();
		if($request->hasfile('cat_img'))
         {
				$po_logoname = time().'.'.$request->file('cat_img')->getClientOriginalExtension();
				$dir = 'public/product/catalog';
				$request->file('cat_img')->move($dir, $po_logoname);
				echo $po_logoname;
			 // echo "1";
            // foreach($request->file('filenames') as $file)
            // {
                // $name = time().'.'.$file->extension();
                // $file->move(public_path().'/product/catalog/', $name);  
                // $data[] = $name;  
            // }
         }
		 if($request->filenames)
		 {
			
			 $po_logoname=$request->filenames;
		 }
	
        // if($request->hasFile('image'))
		// {
			// $image=$request->file('image');
			// $extension=strtolower($request->image->getClientOriginalExtension());
			// if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
			// {
				// $po_logoname = time().'.'.$request->file('image')->getClientOriginalExtension();
				// $dir = 'public/product/catalog';
				// $request->file('image')->move($dir, $po_logoname);
			// }
			// else
			// {
				// return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)'];
				// $po_logoname = "";
			// }
		// }
		// else
		// {
			// return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only pnng, jpg, jpeg allowed)bgh'];
			// $po_logoname = "";
		// }
		if($po_logoname!='')
		{
	    	$category=ucfirst($request->get('category'));
	    	date_default_timezone_set('Asia/Kolkata');
			$create_date=date("Y-m-d");
			$create_time=date("h:i:sa");
			$checkcategory=Catalog::where('cat_name',$category)->first();
			if($checkcategory)
			{
				return ['status' => 'exist', 'redirect' => '', 'msg' =>'Category already exist'];
			}
			else
			{
				$newsl=strtolower($request->get('category'));
				$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $newsl);
				$checkcategorynew=Catalog::where('cat_slug',$slug)->first();
				if($checkcategorynew)
				{
					$newrand=rand(1,9);
					$slug=$slug.$newrand;
				}
				$maxValue = Catalog::max('cat_order');
				$neworder=$maxValue + 1;
				$maindataarr=array('cat_name' => $category,
									'cat_image'=>$po_logoname,
									'cat_slug'=>$slug,
									'seokey'=>$request->get('seokey'),
									'meta_description'=>$request->get('meta_description'),
									'cat_order'=>$neworder,
									'cat_status'=>'1',
									'cat_date'=>$create_date,
									'cat_time'=>$create_time,
								 );
		 
				$datasave=Catalog::insert($maindataarr);
				if($datasave)
				{
					return redirect('admin/list-category');
					return ['status' => 'success', 'redirect' => '', 'msg' =>'Category Added Successfully'];
				}
				else
				{
					return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
				}
			}
		}   
    }
    public function category_list(Request $request)
    {
		$images = \File::allFiles(public_path('/product/catalog'));
        if(session()->has("admin_id"))
        {
            $query=Catalog::where('cat_status','1')->orderBy('cat_id', 'DESC');
	        if($request->has('search'))
	        {
	        	$search=$request->get('search');
	            $query->where(function ($query) use ($search) 
                {
                    $query->orWhere('cat_name', 'like' , "%".$search."%")
                    ->orWhere('cat_slug', 'like' , "%".$search."%")
                    ->orWhere('seokey', 'like' , "%".$search."%")
                    ->orWhere('meta_description', 'like' , "%".$search."%");
                });
	        }
            $cat=$query->paginate(5);
            $data['title']='Category List';
            return view('admin.list-category', compact('data','cat','images'))->render();
        }
        else
        {
            return redirect()->intended('admin');
        }
    }
    public function destroy($id)
    {
        Catalog::where('cat_id', $id)->delete();;
        return redirect()->back()->with('message', 'deleted successfully!');

    }
    public function edit_category($id)
    {   
        $cat = Catalog::where('cat_id', $id)->first();
		$images = \File::allFiles(public_path('/product/catalog'));
	    
		return view('admin.edit-category', compact('cat','images'));
    }
}
