<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersModel extends Model
{
    use SoftDeletes;
    protected $table = 'users';
	public $timestamps = true;
	protected $fillable = ['id','country_id','name','last_name','email','phone','company_name','i_am_a','password'];

}
