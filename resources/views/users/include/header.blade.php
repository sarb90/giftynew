
<meta name="robots" content="noindex, follow" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/vendor/signericafat.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/vendor/cerebrisans.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/vendor/simple-line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/vendor/elegant.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/vendor/linear-icon.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/plugins/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/plugins/easyzoom.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/plugins/slick.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/plugins/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/plugins/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/newstyle.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
#country_id
{

}
.logo img{
    height: 70px;
    margin: 0 auto;
    display: block;
}

.categori-search-wrap {
    height: 44px;
}
.categori-search-wrap .search-wrap-3 form input {
    margin-top: 15px;
}
.categori-search-wrap .search-wrap-3 form button {
    top: 65%;
}
@media screen and (max-width: 786px)
{
    .pb-115
    {
        padding-bottom: 0px !important;
    }
    .mobile-logo img
    {
        height: 50px !important;
    }
}
.header-middle-padding-1 {
    padding: 20px 0 0;
}
</style>
<body>
<header class="header-area">
    <div class="header-large-device">
        <div class="header-top header-top-ptb-1 border-bottom-1">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-7">
                        <div class="social-offer-wrap">
                            <div class="social-style-1">
                                <a href="#"><i class="icon-social-twitter"></i></a>
                                <a href="#"><i class="icon-social-facebook"></i></a>
                                <a href="#"><i class="icon-social-instagram"></i></a>
                                <a href="#"><i class="icon-social-youtube"></i></a>
                                <a href="#"><i class="icon-social-pinterest"></i></a>
                            </div>
                            <div class="header-offer-wrap-2">
                                <p><span>FREE SHIPPING</span> world wide for all orders over $199</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="header-top-right">
                            <div class="same-style-wrap">
                                <div class="same-style same-style-mrg-2 track-order">
                                    <a href="#">Store Location </a>
                                </div>
                                <div class="same-style same-style-mrg-2 currency-wrap">
                                    <a class="currency-dropdown-active" href="#"> USD($) <i class="icon-arrow-down"></i></a>
                                    <div class="currency-dropdown">
                                        <ul>
                                            <li><a href="#">USD</a></li>
                                            <li><a href="#">EUR</a></li>
                                            <li><a href="#">BDT</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="same-style same-style-mrg-2 language-wrap">
                                    <a class="language-dropdown-active" href="#">English <i class="icon-arrow-down"></i></a>
                                    <div class="language-dropdown">
                                        <ul>
                                            <li><a href="#">English</a></li>
                                            <li><a href="#">German</a></li>
                                            <li><a href="#">Spanish</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle header-middle-padding-1">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo">
                            <a href="{{url('/')}}"><img src="assets/images/logo/logo3.png" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                        <div class="categori-search-wrap">
                            <div class="categori-style-1">
                                <select class="nice-select nice-select-style-1">
                                    <option>All Categories </option>
                                    <option>Clothing </option>
                                    <option>T-Shirt</option>
                                    <option>Shoes</option>
                                    <option>Jeans</option>
                                </select>
                                <!--<div class="nice-select nice-select-style-1" tabindex="0">-->
                                <!--    <span class="current">All Categories </span>-->
                                <!--    <ul class="list">-->
                                <!--        <li data-value="All Categories" class="option selected">All Categories </li>-->
                                <!--        <li data-value="Clothing" class="option">Clothing </li>-->
                                <!--        <li data-value="T-Shirt" class="option">T-Shirt</li>-->
                                <!--        <li data-value="Shoes" class="option">Shoes</li>-->
                                <!--        <li data-value="Jeans" class="option">Jeans</li>-->
                                <!--        </ul>-->
                                <!--   </div>-->
                            </div>
                            <div class="search-wrap-3">
                                <form action="#">
                                    <input placeholder="Search Products..." type="text">
                                    <button><i class="lnr lnr-magnifier"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3">
                        <div class="header-action header-action-flex">
                            <div class="same-style-2 same-style-2-font-inc">
                                <a href="{{url('/login-register/')}}"><i class="icon-user"></i></a>
                            </div>
                            <div class="same-style-2 same-style-2-font-inc">
                                <a href="{{url('/wishlist/')}}"><i class="icon-heart"></i><span class="pro-count green">03</span></a>
                            </div>
                            <div class="same-style-2 same-style-2-font-inc header-cart">
                                <a class="cart-active" href="#">
                                    <i class="icon-basket-loaded"></i><span class="pro-count green">02</span>
                                    <span class="cart-amount">$2,435.30</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="header-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="main-categori-wrap main-categori-wrap-index">
                            <a class="categori-show" href="#"><i class="lnr lnr-menu"></i> All Categories<i class="icon-arrow-down icon-right"></i></a>
                            <div class="category-menu category-menu1 categori-hide">
                                <nav>
                                    <ul>
                                        <li class="cr-dropdown"><a href="#">Clothing <span class="icon-arrow-right"></span></a>
                                            <div class="category-menu-dropdown ct-menu-res-height-1">
                                                <div class="single-category-menu ct-menu-mrg-bottom category-menu-border">
                                                    <h4>Men Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Sleeveless shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Trench coat</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cargo pants</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu ct-menu-mrg-bottom ct-menu-mrg-left">
                                                    <h4>Women Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Wedding dress</a></li>
                                                        <li><a href="{{url('/shop/')}}">Gym clothes</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt </a></li>
                                                        <li><a href="{{url('/shop/')}}">Long coat</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu">
                                                    <h4>Kids Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Winter Wear </a></li>
                                                        <li><a href="{{url('/shop/')}}">Occasion Gowns</a></li>
                                                        <li><a href="{{url('/shop/')}}">Birthday Tailcoat</a></li>
                                                        <li><a href="{{url('/shop/')}}">Stylish Unicorn</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu">
                                                    <a href="{{url('/product-details/')}}"><img src="assets/images/menu/menu-categori-1.png" alt=""></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="cr-dropdown"><a href="#">Women <span class="icon-arrow-right"></span></a>
                                            <div class="category-menu-dropdown ct-menu-res-height-2">
                                                <div class="single-category-menu">
                                                    <h4>Men Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Sleeveless shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Trench coat</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cargo pants</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu ct-menu-mrg-left">
                                                    <h4>Women Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Wedding dress</a></li>
                                                        <li><a href="{{url('/shop/')}}">Gym clothes</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt </a></li>
                                                        <li><a href="{{url('/shop/')}}">Long coat</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="cr-dropdown"><a href="#">Men <span class="icon-arrow-right"></span></a>
                                            <div class="category-menu-dropdown ct-menu-res-height-1">
                                                <div class="single-category-menu ct-menu-mrg-bottom category-menu-border">
                                                    <h4>Men Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Sleeveless shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Trench coat</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cargo pants</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu ct-menu-mrg-bottom ct-menu-mrg-left">
                                                    <h4>Women Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Wedding dress</a></li>
                                                        <li><a href="{{url('/shop/')}}">Gym clothes</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt </a></li>
                                                        <li><a href="{{url('/shop/')}}">Long coat</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu">
                                                    <h4>Kids Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Winter Wear </a></li>
                                                        <li><a href="{{url('/shop/')}}">Occasion Gowns</a></li>
                                                        <li><a href="{{url('/shop/')}}">Birthday Tailcoat</a></li>
                                                        <li><a href="{{url('/shop/')}}">Stylish Unicorn</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu">
                                                    <a href="#"><img src="assets/images/menu/menu-categori-1.png" alt=""></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="cr-dropdown"><a href="#">Baby Girl <span class="icon-arrow-right"></span></a>
                                            <div class="category-menu-dropdown ct-menu-res-height-2">
                                                <div class="single-category-menu">
                                                    <h4>Men Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Sleeveless shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt</a></li>
                                                        <li><a href="{{url('/shop/')}}">Trench coat</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cargo pants</a></li>
                                                    </ul>
                                                </div>
                                                <div class="single-category-menu ct-menu-mrg-left">
                                                    <h4>Women Clothing</h4>
                                                    <ul>
                                                        <li><a href="{{url('/shop/')}}">Wedding dress</a></li>
                                                        <li><a href="{{url('/shop/')}}">Gym clothes</a></li>
                                                        <li><a href="{{url('/shop/')}}">Cotton T-shirt </a></li>
                                                        <li><a href="{{url('/shop/')}}">Long coat</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">Baby Boy </a></li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">Accessories </a></li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">Shoes</a></li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">Shirt</a></li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">T-Shirt</a></li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">Coat</a></li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">Jeans</a></li>
                                        <li class="cr-dropdown"><a href="{{url('/shop/')}}">Collection </a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="main-menu main-menu-padding-1 main-menu-font-size-14 main-menu-lh-2">
                            <nav>
                                <ul>
                                    <li><a href="{{url('/')}}">HOME </a>
                                        <!--<ul class="sub-menu-style">-->
                                        <!--    <li><a href="index.html">Home version 1 </a></li>-->
                                        <!--    <li><a href="index-2.html">Home version 2</a></li>-->
                                        <!--    <li><a href="index-3.html">Home version 3</a></li>-->
                                        <!--    <li><a href="index-4.html">Home version 4</a></li>-->
                                        <!--    <li><a href="index-5.html">Home version 5</a></li>-->
                                        <!--    <li><a href="index-6.html">Home version 6</a></li>-->
                                        <!--    <li><a href="index-7.html">Home version 7</a></li>-->
                                        <!--    <li><a href="index-8.html">Home version 8</a></li>-->
                                        <!--    <li><a href="index-9.html">Home version 9</a></li>-->
                                        <!--    <li><a href="index-10.html">Home version 10</a></li>-->
                                        <!--</ul>-->
                                    </li>
                                    <li><a href="{{url('about_us')}}">ABOUT US </a>
                                        <!--<ul class="mega-menu-style mega-menu-mrg-2">-->
                                        <!--    <li>-->
                                        <!--        <ul>-->
                                        <!--            <li>-->
                                        <!--                <a class="dropdown-title" href="#">Shop Layout</a>-->
                                        <!--                <ul>-->
                                        <!--                    <li><a href="shop.html">standard style</a></li>-->
                                        <!--                    <li><a href="shop-list.html">shop list style</a></li>-->
                                        <!--                    <li><a href="shop-fullwide.html">shop fullwide</a></li>-->
                                        <!--                    <li><a href="shop-no-sidebar.html">grid no sidebar</a></li>-->
                                        <!--                    <li><a href="shop-list-no-sidebar.html">list no sidebar</a></li>-->
                                        <!--                    <li><a href="shop-right-sidebar.html">shop right sidebar</a></li>-->
                                        <!--                    <li><a href="store-location.html">store location</a></li>-->
                                        <!--                </ul>-->
                                        <!--            </li>-->
                                        <!--            <li>-->
                                        <!--                <a class="dropdown-title" href="#">Products Layout</a>-->
                                        <!--                <ul>-->
                                        <!--                    <li><a href="product-details.html">tab style 1</a></li>-->
                                        <!--                    <li><a href="product-details-2.html">tab style 2</a></li>-->
                                        <!--                    <li><a href="product-details-sticky.html">sticky style</a></li>-->
                                        <!--                    <li><a href="product-details-gallery.html">gallery style </a></li>-->
                                        <!--                    <li><a href="product-details-affiliate.html">affiliate style</a></li>-->
                                        <!--                    <li><a href="product-details-group.html">group style</a></li>-->
                                        <!--                    <li><a href="product-details-fixed-img.html">fixed image style </a></li>-->
                                        <!--                </ul>-->
                                        <!--            </li>-->
                                        <!--            <li>-->
                                        <!--                <a href="shop.html"><img src="assets/images/banner/banner-12.png" alt=""></a>-->
                                        <!--            </li>-->
                                        <!--        </ul>-->
                                        <!--    </li>-->
                                        <!--</ul>-->
                                    </li>
                                    <li><a href="#">PAGES </a>
                                        <ul class="sub-menu-style">
                                            <!--<li><a href="about-us.html">about us </a></li>-->
                                            <li><a href="cart.html">cart page</a></li>
                                            <li><a href="checkout.html">checkout </a></li>
                                            <li><a href="{{url('my_account')}}">my account</a></li>
                                            <li><a href="wishlist.html">wishlist </a></li>
                                            <li><a href="compare.html">compare </a></li>
                                            <li><a href="contact.html">contact us </a></li>
                                            <li><a href="order-tracking.html">order tracking</a></li>
                                            <li><a href="{{url('/login-register/')}}">login / register </a></li>
                                        </ul>
                                    </li>
                                    <li><a href="blog.html">BLOG </a>
                                        <!--<ul class="sub-menu-style">-->
                                        <!--    <li><a href="blog.html">blog standard </a></li>-->
                                        <!--    <li><a href="blog-no-sidebar.html">blog no sidebar </a></li>-->
                                        <!--    <li><a href="blog-right-sidebar.html">blog right sidebar</a></li>-->
                                        <!--    <li><a href="blog-details.html">blog details</a></li>-->
                                        <!--</ul>-->
                                    </li>
                                    <li><a href="contact.html">CONTACT </a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="hotline">
                            <p><i class="icon-call-end"></i> <span>Hotline</span> (364) 106 7572 </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       
    <div class="header-small-device small-device-ptb-1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-5">
                    <div class="mobile-logo">
                        <a href="{{url('/')}}">
                            <img alt="" src="assets/images/logo/logo3.png">
                        </a>
                    </div>
                </div>
                <div class="col-7">
                    <div class="header-action header-action-flex">
                        <div class="same-style-2 same-style-2-font-inc">
                            <a href="{{url('/login-register/')}}"><i class="icon-user"></i></a>
                        </div>
                        <div class="same-style-2 same-style-2-font-inc">
                            <a href="{{url('/wishlist/')}}"><i class="icon-heart"></i><span class="pro-count green">03</span></a>
                        </div>
                        <div class="same-style-2 same-style-2-font-inc header-cart">
                            <a class="cart-active" href="#">
                                <i class="icon-basket-loaded"></i><span class="pro-count green">02</span>
                            </a>
                        </div>
                        <div class="same-style-2 main-menu-icon">
                            <a class="mobile-header-button-active" href="#"><i class="icon-menu"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
</body>