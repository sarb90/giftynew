<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="{{('assets/img/basic/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Registeration</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{('assets/css/app.css')}}">
      <link rel="stylesheet" href="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css')}}">
    <style>
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }
    </style>
</head>
<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div> 
<div id="app">
    <main id="main" class="site-main">
        <div class="container">
            <div class="col-xl-8 mx-lg-auto p-t-b-80">
                <header class="text-center">
                    <h1>Create New Account</h1>
                    <p>Join Our wonderful community and let others help you without a single penny</p>
                    <img class="p-t-b-50" src="{{('assets/img/icon/icon-join.png') }}" alt="">
                </header>
                <form class="btn-submit" id="add_form" method="POST">
                    <!-- @csrf -->
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="{{url('/redirect')}}" class="btn btn-primary">Login with Facebook</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="{{ url('/redirectgmail') }}" class="btn btn-primary">Login With Google</a>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <select class="form-control textfield" name="country_id" id="country_id" autofocus>
                                    <option value="">Select Country/Region</option>
                                    @foreach($countries as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger" id="country_id_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group" id="i_am_a">
                                <lable class="col-lg-12">I am A:</lable>
                                <input type="radio" id="supplier" name="i_am_a" value="supplier">
                                <label for="supplier">Supplier</label>
                                <input type="radio" id="buyer" name="i_am_a" value="buyer">
                                <label for="buyer">Buyer</label>
                                <input type="radio" id="both" name="i_am_a" value="both">
                                <label for="both">Both</label>
                                <span class="text-danger" id="i_am_a_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="name" id="name" class="form-control form-control-lg textfield" placeholder="First Name">
                                <span class="text-danger" id="name_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text"  name="last_name" id="last_name"  class="form-control form-control-lg textfield" placeholder="Last Name">
                                <span class="text-danger" id="last_name_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="text"  name="company_name" id="company_name" class="form-control form-control-lg textfield" placeholder="Company Name">
                                <span class="text-danger" id="company_name_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="text"  name="phone" id="phone" class="form-control form-control-lg textfield" maxlength="10" placeholder="Phone">
                                <span class="text-danger" id="phone_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="text" name="email" id="email" class="form-control form-control-lg textfield" placeholder="Email Address">
                                <span class="text-danger" id="email_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="password"  name="password" id="password" class="form-control form-control-lg textfield" placeholder="Password">
                                <span toggle="#password"  class="fa fa-fw fa-eye  field-icon toggle-password"></span>
                                <span class="text-danger" id="password_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="password" name="conf_pwd" id="conf_pwd" class="form-control form-control-lg textfield" placeholder="Confirm Password">
                                <span toggle="#conf_pwd"  class="fa fa-fw fa-eye  field-icon toggle-password"></span>
                                <span class="text-danger" id="conf_pwd_err" style="color:red;"></span><br><br>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text"  name="captcha" id="captcha" class="form-control form-control-lg textfield" placeholder="Captcha">
                                <span class="text-danger" id="captcha_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <?php $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; ?>
                                <input type="text" name="captcha1" id="captcha1" class="form-control form-control-lg" readonly value="<?php echo substr(str_shuffle($permitted_chars), 0, 6); ?>" oncopy="return false" onpaste="return false" oncut="return false">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <button type="button" name="btnRefresh" id="btnRefresh" class="btnRefresh">Refresh Captcha</button><br><br>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <input type="button" id="submit" class="btn btn-success btn-lg btn-block" value="Register Now">
                            <p class="forget-pass">A verification email wil be sent to you</p>
                        </div>
                    </div>  
                </form>
            </div>
        </div>
    </main>
</div>
<!--/#app -->
<script src="assets/js/app.js"></script>
<script src="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
$(document).on('click','.btnRefresh',function()
{
    $.ajax(
    {
        url: '{{route("captchacode")}}',
        type: "POST",
        data: 
        {
            "_token": "{{ csrf_token() }}",
        },
        success: function(data) 
        {
            $('#captcha1').val(data);
        }
    });
});
$(document).on('blur','.textfield',function()
{
    if($(this).val().trim()=="")
    {
        $("#"+this.id+"_err").text("Please fill this field");
        $("#"+this.id+"_err").show();
    }
    else
    {
        var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(this.id=="phone" && $(this).val().trim().length<10)
        {
            $("#"+this.id+"_err").text("Phone should be of 10-digits");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="phone" && $(this).val().trim().length>=10)
        {
            var formdata="phone="+$('#phone').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("phone_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#phone_err").text("Phone already exist");
                        $("#phone_err").show();
                    }
                    else
                    {
                        $("#phone_err").text("");
                        $("#phone_err").hide();
                    }
                }
            });
        }
        else if(this.id=="email" && !filter.test($(this).val().trim()))
        {
            $("#"+this.id+"_err").text("Invalid Email");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="email" && filter.test($(this).val().trim()))
        {
            var formdata="email="+$('#email').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("email_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#email_err").text("Email already exist");
                        $("#email_err").show();
                    }
                    else
                    {
                        $("#email_err").text("");
                        $("#email_err").hide();
                    }
                }
            });
        }
        else if(this.id=="password" && $('#conf_pwd').val().trim()!='' && $('#'+this.id).val().trim()!=$('#conf_pwd').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="conf_pwd" && $('#password').val().trim()!='' && $('#'+this.id).val().trim()!=$('#password').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="captcha" && $('#'+this.id).val().trim()!=$('#captcha1').val().trim())
        {
            $("#"+this.id+"_err").text("Invalid Captcha");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }
});
$(document).on('keyup change','.textfield',function()
{
    if($(this).val().trim()=="")
    {
        $("#"+this.id+"_err").text("Please fill this field");
        $("#"+this.id+"_err").show();
    }
    else
    {
        var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(this.id=="phone" && $(this).val().trim().length<10)
        {
            $("#"+this.id+"_err").text("Phone should be of 10-digits");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="phone" && $(this).val().trim().length>=10)
        {
            var formdata="phone="+$('#phone').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("phone_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#phone_err").text("Phone already exist");
                        $("#phone_err").show();
                    }
                    else
                    {
                        $("#phone_err").text("");
                        $("#phone_err").hide();
                    }
                }
            });
        }
        else if(this.id=="email" && !filter.test($(this).val().trim()))
        {
            $("#"+this.id+"_err").text("Invalid Email");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="email" && filter.test($(this).val().trim()))
        {
            var formdata="email="+$('#email').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("email_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#email_err").text("Email already exist");
                        $("#email_err").show();
                    }
                    else
                    {
                        $("#email_err").text("");
                        $("#email_err").hide();
                    }
                }
            });
        }
        else if(this.id=="password" && $('#conf_pwd').val().trim()!='' && $('#'+this.id).val().trim()!=$('#conf_pwd').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="conf_pwd" && $('#password').val().trim()!='' && $('#'+this.id).val().trim()!=$('#password').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="captcha" && $('#'+this.id).val().trim()!=$('#captcha1').val().trim())
        {
            $("#"+this.id+"_err").text("Invalid Captcha");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }
});
$(document).on('click','#submit',function()
{
    $('.text-danger').hide();
    $('.text-danger').val('');
    var isValid = true;
    $('.textfield').each(function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
            isValid = false;        
        }
        else
        {
            var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(this.id=="phone" && $(this).val().trim().length<10)
            {
                $("#"+this.id+"_err").text("Phone should be of 10-digits");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="email" && !filter.test($(this).val().trim()))
            {
                $("#"+this.id+"_err").text("Invalid Email");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="password" && $('#conf_pwd').val().trim()!='' && $('#'+this.id).val().trim()!=$('#conf_pwd').val().trim())
            {
                $("#"+this.id+"_err").text("Password should be match with confirm password");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="conf_pwd" && $('#password').val().trim()!='' && $('#'+this.id).val().trim()!=$('#password').val().trim())
            {
                $("#"+this.id+"_err").text("Password should be match with confirm password");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="captcha" && $('#'+this.id).val().trim()!=$('#captcha1').val().trim())
            {
                $("#"+this.id+"_err").text("Invalid Captcha");
                $("#"+this.id+"_err").show();
            }
        }
    });
    if ($('input[name="i_am_a"]:checked').length == 0)
    {
        $("#i_am_a_err").text("Please select this field");
        $("#i_am_a_err").show();
        isValid = false;
    }
    if(isValid)
    {
        var formdata=$("#add_form").serialize()+"&_token={{ csrf_token() }}";
        $.ajax(
        {
            url: '{{route("register_user")}}',
            type: "POST",
            data: formdata,
            success: function(data) 
            {
                if(data.status=='success')
                {
                    Swal.fire({
                    title: "Successfully Added",
                    text: data.msg,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false
                    });
                    window.location.href='{{route("login")}}';
                }
                else
                {
                    Swal.fire({
                    title: "Notice",
                    text: data.msg,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    dangerMode: true,
                    });
                }
            },
        });
    }
    else
    {
        Swal.fire({
        title: "Notice",
        text: 'Please fill all required fields',
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        cancelButtonText:false,
        closeOnConfirm: false,
        closeOnCancel: false,
        dangerMode: true,
        });
    }
});
$(function()
{
    $("#phone").keypress(function(event) 
    {
        return /\d/.test(String.fromCharCode(event.keyCode));
    });
});
$(".toggle-password").click(function() 
{
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") 
    {
        input.attr("type", "text");
    } 
    else 
    {
        input.attr("type", "password");
    }
});
</script>
</body>
</html>