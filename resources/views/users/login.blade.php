<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="assets/img/basic/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Vendor Login</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{('assets/css/app.css')}}">
      <link rel="stylesheet" href="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css')}}">
    <style>
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }
    </style>
</head>
<body class="light">
<!-- Pre loader -->
 <div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div> 
<div id="app">
<main>
    <div id="primary" class="p-t-b-100 height-full ">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mx-md-auto">
                    <div class="text-center">
                        <img src="assets/img/dummy/u5.png" alt="">
                        <h3 class="mt-2">Welcome Back</h3>
                        <p class="p-t-b-20">Hey Soldier welcome back signin now there is lot of new stuff waiting
                            for you</p>
                    </div>
                    <form id="login_form" method="post" >
                         <!-- @csrf -->
                        <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                            <input type="text" name="email" id="email" class="form-control form-control-lg textfield"
                                   placeholder="Email Address" required="">
                        </div>
                        <div class="form-group has-icon"><i class="icon-user-secret"></i>
                            <input type="password" name="password" id="password" class="form-control form-control-lg textfield"
                                   placeholder="Password" required="">
                        </div>
                        <input type="button" class="btn btn-success btn-lg btn-block" id="login-form" name="submit" value="Log In">
                        <p class="forget-pass">Have you forgot your username or password ?</p>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="{{url('/redirect')}}" class="btn btn-primary">Login with Facebook</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="{{ url('/redirectgmail') }}" class="btn btn-primary">Login With Google</a>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #primary -->
</main>
</div>
<!--/#app -->
<script src="assets/js/app.js"></script>
<script src="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
$(document).on('click','#login-form',function()
{
    $('.text-danger').hide();
    $('.text-danger').val('');
    var isValid = true;
    $('.textfield').each(function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
            isValid = false;        
        }
    });
    if(isValid)
    {
        var formdata=$("#login_form").serialize()+"&_token={{ csrf_token() }}";
        $.ajax(
        {
            url: '{{route("login_user")}}',
            type: "POST",
            data: formdata,
            success: function(data) 
            {
                if(data.status=='success')
                {
                    Swal.fire({
                    title: "Successfully Login",
                    text: data.msg,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false
                    });
                    window.location.href='{{route("catalog")}}';
                }
                else
                {
                    Swal.fire({
                    title: "Notice",
                    text: data.msg,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    dangerMode: true,
                    });
                }
            },
        });
    }
    else
    {
        Swal.fire({
        title: "Notice",
        text: 'Please fill all required fields',
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        cancelButtonText:false,
        closeOnConfirm: false,
        closeOnCancel: false,
        dangerMode: true,
        });
    }
});
</script>

</body>
</html>