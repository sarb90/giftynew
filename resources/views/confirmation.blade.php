@include('users.include.header')
<div class="breadcrumb-area bg-gray">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="{{url('/')}}">Checkout</a>
                </li>
                @if(!empty($data))
                <li class="active">{{$data['title']}}</li>
                @endif
            </ul>
        </div>
    </div>
</div>
<section class="order-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto container123">
                <div class="row border-bottom">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="order-logo">
                        <img src="https://events.oneeyegps.com/gifty/assets/images/logo/logo3.png">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="order-confirm" align="right">
                        <h3>Order Confirmation</h3>
                    </div>
                </div>
                </div>
                <div class="order-content ">
                    <h4>Hello <span class="text-body">Sarb Singh</span></h4>
                    <p>Thank you for shopping with us. You ordered <span>
                      </span> We'll send a confirmation when your items ships.
                    </p>
                    <h4>Details</h4>
                    <p>Order <span>NAU051</span>
                    </p>
                </div>
                <div class="row order-row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="order-left">
                            <span> Arriving:</span>
                            <p>Tuesday, January 19</p>
                            <a class="btn" href="https://hiraola.oneeyegps.com/invoice_pdf/NTE=">
                                View or manage order<i class="icon-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="order-right">
                            <span>Ship to:</span>
                            <p class="text-body">test address Bala Morghab<br> Badgis Afghanistan 3345566</p>
                            <ul class="order-payment">
                                <li>
                                    <span class="left">Total Before Tax:</span>
                                    <span class="right"> $761.00 </span>
                                </li>
                                    <span class="left">Tax (0%)</span>
                                    <span class="right">$0.00 </span>
                                </li>
                                 <li>
                                    <span class="left">Shipping </span>
                                    <span class="right">$5.00</span>
                                 </li>
                                 <li>
                                    <span class="left"><b>Order Total:</b></span>
                                    <span class="right"><b>$766.00</b></span>
                                </li>
                            </ul>
                        </div>
                   </div>
                
                </div>
            </div>
        </div>
    </div>
</section>

@include('users.include.footer')