 @extends('layouts.app')
 
    @section('content')
		<style>
	[type=radio] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
  display: inline-grid;
}

/* IMAGE STYLES */
[type=radio] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=radio]:checked + img {
  outline: 2px solid #f00787;
  
}
	</style>
     <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>Add Category</h4>
                                                       <!--  <span>Lorem ipsum dolor sit <code>amet</code>, consectetur
                                                            adipisicing elit</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item"  style="float: left;">
                                                            <a href="{{url('/admin/')}}"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Master</a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Add Category</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
										<div class="col-lg-12">

                        <div class="card card-outline-info">

                            <div class="card-header">

                                <h4 class="m-b-0 text-white">Category</h4>

                            </div>

                            <div class="card-body">

                                <form id="form" method="post" action="{{ url('/admin/add-category/create')}}"  enctype="multipart/form-data">
								@csrf
                                    <div class="form-body">
                                        <!-- <h3 class="card-title">Franchisee Details</h3> -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Enter Name <span class="text-danger">*</span></label>
                                                    <input type="text" name="category" id="category" value="{{ $cat->cat_name }}" class="form-control category capitalise">
                                                    <small class="has_error" id="category_error"> This Field is Required </small>
                                                 </div>
                                            </div>
                                             <div class="col-md-4">
											
											
									<div class="col-md-12 mb-2 img_show" >
									<img id="image_preview_container" src="{{ url('product/catalog/'.$cat->cat_image) }}""
										alt="preview image" style="max-height: 150px;">
										<span data-toggle="modal" data-target="#myModal">Chnage Image</span>
									</div>
											</div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Seo Keywords</label>
                                                    <input type="text" name="seokey" id="seokey" value="{{ $cat->seokey }}" class="form-control seokey">
                                                    <small class="has_error" id="seokey_error"> This Field is Required </small>
                                                 </div>
                                            </div>
                                             <div class="col-md-12"><grammarly-extension data-grammarly-shadow-root="true" style="position: absolute; top: 0px; left: 0px; pointer-events: none;" class="cGcvT"></grammarly-extension>
                                                <div class="form-group">
                                                    <label class="control-label">Meta Description </label>
                                                   <textarea class="form-control" id="meta_description"  value="{{ $cat->meta_description }}" name="meta_description" spellcheck="false"></textarea>
                                                 </div>
                                            </div>
											<input type="submit" class="btn btn-success" value="add Category">
                                        </div>
                            </div>
													<div id="myModal" class="modal fade" role="dialog">
						  <div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
							  <div class="modal-header">
							   
								<h4 class="modal-title">Select Image</h4>
								<!-- <button type="button" class="btn btn-primary upload-img">Uploads</button>-->
								
								 <label class="btn btn-default">
									<span class="btn btn-primary "style="">Upload</span> <input type="file" id="imageU" style="" name="cat_img" hidden>
												<span class="text-danger">{{ $errors->first('title') }}</span>	
								</label>
							  </div>
							 <div class="modal-body">
							  <div class="row">
							  @foreach ($images as $image)
								<div class="card col-md-3"> 
								<label>
								   <input type="radio" name="filenames" value="{{$image->getFilename()}}" checked>
									
									<img src="{{ asset('product/catalog/' . $image->getFilename()) }}" class="imgsrcst" width="50" height="50">
									
								</label>
									
										<div class="card-body"> 
											<p class="card-text">{{$image->getFilename()}}</p> 
										</div> 
								</div> 
								@endforeach
								</div>
								
							  </div>
							  <div class="modal-footer">
							   <button type="button" class="btn btn-success inserttopost" data-dismiss="modal">inseet to post</button>
							   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							   <div class="row">
										
										<div class="col-md-12">
											<div class="form-group">
											
											</div>
										</div>
							  </div>
							</div>

						  </div>
						</div>
						</form>
							</div> 

							</form>
                           </div> 
                           <!--table end-->
                    </div>
                </div>
                                         
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
    @endsection
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
          $(document).ready(function() {
	$(document).ready(function (e) {
		
	 $(document).on('click','.inserttopost',function()
		{
			//var isrc= $('.imgsrcst').attr('src');
		 var rad = $('input[name=filenames]:checked').val();
		var source = "{!! asset('product/catalog/') !!}";
		var aa = source + '/'+rad;
	  $('.img_show').show();
		$('#image_preview_container').attr('src', aa);
		
	});
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
 
        $('#imageU').change(function(){
          
            let reader = new FileReader();
            reader.onload = (e) => { 
              $('#image_preview_container').attr('src', e.target.result); 
			  $('.img_show').show();
			  $('#myModal').modal('toggle');
            }
            reader.readAsDataURL(this.files[0]); 
 
        });
 
        $('#upload_image_form').submit(function(e) {
            e.preventDefault();
 
            var formData = new FormData(this);
 
            $.ajax({
                type:'POST',
                url: "{{ url('save-photo')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    this.reset();
                    alert('Image has been uploaded successfully');
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
    });
 
            $('pre code').each(function(i, block) {
                hljs.highlightBlock(block);
              });

            $('#summernote').summernote();

            CKEDITOR.replace( 'editor' );
            var oEditor = CKEDITOR.instances.editor;

            var mediamanager = new Mediamanager({
                loadItemsUrl: 'loadfiles.json',
            });

            document.getElementById('summernoteMedia').addEventListener('click', function () {
               mediamanager.open();
               mediamanager.options.insertType = 'html';
               mediamanager.options.insert = function (data) {
                   $('#summernote').summernote('insertNode', data);
               }
            });

            $('#ckeditorMedia').click(function () {
               mediamanager.open();
               mediamanager.options.insertType = 'string';
               mediamanager.options.insert = function (data) {
                    var newElement = CKEDITOR.dom.element.createFromHtml( data, oEditor.document );
                    oEditor.insertElement( newElement );
               }
            });

            var mediamanager2 = new Mediamanager({
                buttonText: 'Insert to Post',
                deleteButtonText: 'Delete',
                deleteConfirmationText: 'Are you sure?',
                deleteItemUrl: '/',
                loadItemsUrl: 'loadfiles.json',
                loadNextItemsUrl: 'loadnextfiles.json',
                loadNextItemDelay: 3000,
                uploadUrl: '/',
                insertType: 'object',
                insert: function (data) {
                    console.log(data);
                }
            });

            $('#allOptionsButton').click(function () {
               mediamanager2.open();
			});
			});
    </script>