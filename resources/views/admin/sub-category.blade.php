 @extends('layouts.app')
 
    @section('content')
     <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>Add Sub Category</h4>
                                                       <!--  <span>Lorem ipsum dolor sit <code>amet</code>, consectetur
                                                            adipisicing elit</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item"  style="float: left;">
                                                            <a href="{{url('/admin/')}}"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Master</a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Add Category</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
										<div class="col-lg-12">

                        <div class="card card-outline-info">

                            <div class="card-header">

                                <h4 class="m-b-0 text-white">Sub Category</h4>

                            </div>

                            <div class="card-body">

                             <form id="needs-validation" class="subcatalog_form" novalidate enctype="multipart/form-data">
								@csrf
                                    <div class="form-body">
                                        <!-- <h3 class="card-title">Franchisee Details</h3> -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="category">Category*</label>
														<select id="category" name="category" class="custom-select form-control textfield" required autofocus>
															<option value="">Select Product Category</option>
															@foreach($productdata as $prolist)
																<option value="{{$prolist->cat_id}}">{{$prolist->cat_name}}</option>
															@endforeach
														</select>
														<span class="text-danger" id="category_err" style="color:red;"></span>
                                                 </div>
                                            </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="subcategory">Sub Category Name*</label>
													<input type="text" class="form-control textfield" id="subcategory" name="subcategory" placeholder="Category Name" required>
													<span class="text-danger" id="subcategory_err" style="color:red;"></span>
                                                 </div>
                                            </div>

                                           
                                        </div>
                                        <!--/row-->
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                   <label for="seokey">SEO Keywords</label>
													<input type="text" class="form-control" id="seokey" name="seokey" placeholder="SEO Keywords" required>
													<span class="text-danger" id="seokey_err" style="color:red;"></span>
                                                 </div>
                                            </div>
                                             <div class="col-md-12"><grammarly-extension data-grammarly-shadow-root="true" style="position: absolute; top: 0px; left: 0px; pointer-events: none;" class="cGcvT"></grammarly-extension>
                                                <div class="form-group">
                                                   <label for="meta_description">Meta Description</label>
													<textarea class="form-control p-t-20 editor" id="meta_description" name="meta_description"
															  placeholder="Write Something..." rows="7" required></textarea>
													<span class="text-danger" id="meta_description_err" style="color:red;"></span>
                                                 </div>
                                            </div>
											 <button class="btn btn-primary" type="button" id="publish">Submit</button>
                                        </div>
                            </div>
										
							</form>
                           </div> 
                           <!--table end-->
                    </div>
                </div>
	

                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
    @endsection
<script src="{{ asset('vendor-assets/js/app.js') }}"></script>
<script>
    $(document).on('blur','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    });
    $(document).on('keyup change','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }); 
    $(document).on('click','#publish',function()
    {
		
        $('.text-danger').hide();
        $('.text-danger').val('');
        var isValid = true;
        $('.textfield').each(function()
        {
            if($(this).val().trim()=="")
            {
                $("#"+this.id+"_err").text("Please fill this field");
                $("#"+this.id+"_err").show();
                isValid = false;        
            }
        });
        if(isValid)
        {
            var formData=$(".subcatalog_form").serialize();
			
            $.ajax(
            {
                url: "{{ route('add_subcategory') }}",
                type: 'POST',
                data: formData,
                success: function(data) 
                {
					window.location.href = "{{ url('admin/subcategory-list')}}";
                    if(data.status=='success')
                    {
                        Swal.fire({
                        title: "Successfully Added",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false
                        });
                        $(".subcatalog_form")[0].reset();
                        $('#category').focus();
                    }
                    else
                    {
                        Swal.fire({
                        title: "Notice",
                        text: data.msg,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        dangerMode: true,
                        });
                    }
                },
            });
        }
        else
        {
            Swal.fire({
            title: "Notice",
            text: 'Please fill all required fields',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText:false,
            closeOnConfirm: false,
            closeOnCancel: false,
            dangerMode: true,
            });
        }
    });
</script>