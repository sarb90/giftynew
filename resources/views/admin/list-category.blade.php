 @extends('layouts.app')
 
    @section('content')
     <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>List Category</h4>
                                                       <!--  <span>Lorem ipsum dolor sit <code>amet</code>, consectetur
                                                            adipisicing elit</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item"  style="float: left;">
                                                            <a href="{{url('/admin/')}}"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Master</a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Add Category</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
										<div class="col-lg-12">

                        <div class="card card-outline-info">

                            <div class="card-header">

                                <h4 class="m-b-0 text-white">List Category</h4>

                            </div>

                            <div class="card-body">
							@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Sr No.</th>
                <th>Category</th>
				<th>Slug</th>
				<th>Seo Key</th>
				<th>Meta Description</th>
				<th>Cat Image</th>
				<th>Action</th>
            </tr>
        </thead>
        <tbody>
		  @if(count($cat)>0)
			<?php $i=1; ?>
			@foreach($cat as $cats)
				<tr>
					<td>{{$i}}</td>
					<td id="cat_name{{$cats->cat_id}}">{{$cats->cat_name}}</td>
					<td id="cat_slug{{$cats->cat_id}}">{{$cats->cat_slug}}</td>
					<td id="cat_seokey{{$cats->cat_id}}">{{$cats->seokey}}</td>
					<td id="cat_meta_description{{$cats->cat_id}}">{{$cats->meta_description}}</td>
					<td id="cat_img{{$cats->cat_id}}"><img src="{{ asset('product/catalog/') }}/{{$cats->cat_image}}" style="heigt:100px;width:100px;"></td>
					<td>
					<a href="{{ url('admin/edit-category/'.$cats->cat_id)}}"><span class="edit btn btn-success" id="{{$cats->cat_id}}">Edit</span></a>
					<a href="{{ url('/admin/category/delete/'.$cats->cat_id)}}"><span class="delete btn btn-primary" id="{{$cats->cat_id}}">Delete</span></a>
					<a href="{{ url('#')}}"><span class="actived btn btn-secondary" id="{{$cats->cat_id}}">Active</span></a>
					</td>
				</tr>
				<?php $i++; ?>
			@endforeach
		@else
			<tr>
				<th colspan="7">No Data Found</th>
			</tr>
		@endif
		</tbody>
          
    </table>
                           </div> 
                           <!--table end-->
                    </div>
                </div>
                                        
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
    @endsection
