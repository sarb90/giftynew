 @extends('layouts.app')
 
    @section('content')
     <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>List Sub Category</h4>
                                                       <!--  <span>Lorem ipsum dolor sit <code>amet</code>, consectetur
                                                            adipisicing elit</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item"  style="float: left;">
                                                            <a href="{{url('/admin/')}}"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Master</a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Add Category</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
										<div class="col-lg-12">

                        <div class="card card-outline-info">

                            <div class="card-header">

                                <h4 class="m-b-0 text-white">List Sub Category</h4>

                            </div>

                            <div class="card-body">
							@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                 <th style="width: 10px">#</th>
				<th>Category</th>
				<th>Sub Category</th>
				<th>Slug</th>
				<th>Seo Key</th>
				<th>Meta Description</th>
				<th>Action</th>
            </tr>
        </thead>
        <tbody>
		   @if(count($scat)>0)
			<?php $i=1; ?>
			@foreach($scat as $scats)
				<tr>
					<td>{{$i}}</td>
					<td id="scat_cat{{$scats->scat_id}}">{{$scats->cat_name}}</td>
					<td id="scat_name{{$scats->scat_id}}">{{$scats->scat_name}}</td>
					<td id="scat_slug{{$scats->scat_id}}">{{$scats->cat_slug}}</td>
					<td id="scat_seokey{{$scats->scat_id}}">{{$scats->seokey}}</td>
					<td id="scat_meta_description{{$scats->scat_id}}">{{$scats->meta_description}}</td>
					<td>
					<span class="edit btn btn-success edit" id="{{$scats->scat_id}}">Edit</span>
					<span class="deleteRecord btn btn-success "  data-id="{{$scats->scat_id}}">Delete</span>
					</td>
				</tr>
				<?php $i++; ?>
			@endforeach
		@else
			<tr>
				<th colspan="5">No Data Found</th>
			</tr>
		@endif
		</tbody>
          
    </table>
	
                           </div> 
                           <!--table end-->
                    </div>
                </div>
                                        
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Sub Category</h4>
      </div>
      <div class="modal-body">
            <form id="needs-validation" class="subcatalog_form" novalidate enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="category">Category*</label>
                                <input type="hidden" id="scat_id" name="scat_id">
                                <select id="category" name="category" class="custom-select form-control textfield" required autofocus>
                                    <option value="">Select Product Category</option>
                                    @foreach($productdata as $prolist)
                                        <option value="{{$prolist->cat_id}}">{{$prolist->cat_name}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger" id="category_err" style="color:red;"></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="subcategory">Sub Category Name*</label>
                                <input type="text" class="form-control textfield" id="subcategory" name="subcategory" placeholder="Category Name" required>
                                <span class="text-danger" id="subcategory_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-6">
                                <label for="seokey">SEO Keywords</label>
                                <input type="text" class="form-control" id="seokey" name="seokey" placeholder="SEO Keywords" required>
                                <span class="text-danger" id="seokey_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-6">
                                <label for="meta_description">Meta Description</label>
                                <textarea class="form-control p-t-40 editor" id="meta_description" name="meta_description"
                                          placeholder="Write Something..." rows="3" required></textarea>
                                <span class="text-danger" id="meta_description_err" style="color:red;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card mt-4">
                            
                            <div class="card-footer bg-transparent">
                                <button class="btn btn-primary" type="button" id="publish">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
                        </div>
                    </div>
    @endsection
	<script src="{{ asset('vendor-assets/js/app.js') }}"></script>
<script>
    $(window).on('hashchange', function() 
    {
        if (window.location.hash) 
        {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) 
            {
                return false;
            }
            else
            {
                getData(page);
            }
        }
    });
    $(document).ready(function()
    {
		
        $(document).on('click', '.pagination a',function(event)
        {
            event.preventDefault();
            var page=$(this).attr('href').split('page=')[1];
            getData(page);
        });
    });
    function getData(page)
    {
        var search=$('#search').val();
        $.ajax(
        {
            url: '?page=' + page+'&search='+search,
            type: "get",
            datatype: "html"
        }).done(function(data)
        {
            var data1=jQuery(data).find('.result_append').html();
            $(".result_append").empty().html(data1);
            location.hash = page;
        }).fail(function(jqXHR, ajaxOptions, thrownError)
        {
          alert('No response from server');
      });
    }
    $(document).on('change','#cat',function(e)
    {
        do_stuff();
    });
    $(document).on('keyup','#search',function(e)
    {
        do_stuff();
    });
    function do_stuff()
    {
        var search=$('#search').val();
        var cat=$('#cat').val();
        $.ajax(
        {
            url: '{{route("subcategory_list")}}',
            data: {
            'search':search,
            'cat':cat,
            '_token':'{{csrf_token()}}'
            },
            type: 'POST',
            success: function (data) 
            {
            var data1=jQuery(data).find('.result_append').html();
            $(".result_append").empty().html(data1);
            }
        });
    }
    $(document).on('click','.edit',function()
    {
		
        var id=this.id;
        $.ajax(
        {
            url: '{{route("fetch_scat_data")}}',
            data: {
            'scat_id':this.id,
            '_token':'{{csrf_token()}}'
            },
            type: 'POST',
            dataType: "json",
            success: function (data) 
            {
                $('#scat_id').val(data.scat_id);
                $('#category').val(data.cat_id);
                $('#subcategory').val(data.scat_name);
                $('#seokey').val(data.seokey);
                $('#meta_description').val(data.meta_description);
                $('#editModal').modal('show');
            }
        });
    });
    $(document).on('blur','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    });
    $(document).on('keyup change','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }); 
    $(document).on('click','#publish',function()
    {
        $('.text-danger').hide();
        $('.text-danger').val('');
        var isValid = true;
        $('.textfield').each(function()
        {
            if($(this).val().trim()=="")
            {
                $("#"+this.id+"_err").text("Please fill this field");
                $("#"+this.id+"_err").show();
                isValid = false;        
            }
        });
        if(isValid)
        {
            var formData=$(".subcatalog_form").serialize();
            $.ajax(
            {
                url: "{{ route('edit_subcategory') }}",
                type: 'POST',
                data: formData,
                success: function(data) 
                {
                    if(data.status=='success')
                    {
                        Swal.fire({
                        title: "Successfully Updated",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false
                        });
                        var id=$('#scat_id').val().trim();
                        var category=$( "#category option:selected" ).text();
                        $('#scat_cat'+id).text(category);
                        $('#scat_name'+id).text($('#subcategory').val().trim());
                        $('#scat_seokey'+id).text($('#seokey').val().trim());
                        $('#scat_meta_description'+id).text($('#meta_description').val().trim());
                        $(".subcatalog_form")[0].reset();
                        $('#editModal').modal('hide');
                    }
                    else
                    {
                        Swal.fire({
                        title: "Notice",
                        text: data.msg,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        dangerMode: true,
                        });
                    }
                },
            });
        }
        else
        {
            Swal.fire({
            title: "Notice",
            text: 'Please fill all required fields',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText:false,
            closeOnConfirm: false,
            closeOnCancel: false,
            dangerMode: true,
            });
        }
    });
	$(document).on('click','.deleteRecord',function()
    {
	alert();
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
   
    $.ajax(
    {
        url: "destroy_sub_cat/"+id,
        type: 'DELETE',
        data: {
            "id": id,
             '_token':'{{csrf_token()}}'
        },
        success: function (){
            if(data.status=='success')
                    {
                        Swal.fire({
                        title: "Deleted Successfully",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false
                        });
                      
                    }
        }
    });
   
});
</script>