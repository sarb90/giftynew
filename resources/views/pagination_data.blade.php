 <div class="table-responsive ">
    <table class="table table-striped table-hover r-0">

        <thead>
        <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Company Name</th>
            <th>I Am A</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
            @foreach($data as $row)
       
        <tr>
            <td>{{ $row->id }}</td>
            <td>{{ $row->name }}</td>
            <td>{{ $row->last_name }}</td>
            <td>{{ $row->email }}</td>
            <td>{{ $row->phone }}</td>
            <td>{{ $row->company_name }}</td>
            <td>{{ $row->i_am_a }}</td>
            <td>
            
                <a href="editVendor/{{ $row->id }}"><i class="icon-pencil"></i></a>
                <a href="deleteVendor/{{ $row->id }}" onClick="return confirm('Are you sure you want to delete?');"><i class="icon-delete"></i></a>
            </td>
         </tr>
      @endforeach
      </tbody>
    </table>
       {{ $data->links() }}
  

</div>
